package net.dona.doip.client.http;

import net.dona.doip.InDoipMessage;
import net.dona.doip.InDoipSegment;
import net.dona.doip.InDoipSegmentFromInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class InDoipMessageFromHttpResponse implements InDoipMessage {

    private final CloseableHttpResponse httpResponse;
    private final HttpEntity entity;
    private final InputStream in;
    private final Spliterator<InDoipSegment> spliterator;

    public InDoipMessageFromHttpResponse(CloseableHttpResponse httpResponse) throws IOException {
        this.httpResponse = httpResponse;
        entity = httpResponse.getEntity();
        in = entity.getContent();
        boolean isJson = isJsonResponse(httpResponse);
        InDoipSegment segment = new InDoipSegmentFromInputStream(isJson, in);
        List<InDoipSegment> segments = Collections.singletonList(segment);
        this.spliterator = Spliterators.spliterator(segments, Spliterator.IMMUTABLE | Spliterator.ORDERED);
    }

    private static boolean isJsonResponse(CloseableHttpResponse httpResponse) {
        Header contentTypeHeader = httpResponse.getFirstHeader(HttpHeaders.CONTENT_TYPE);
        if (contentTypeHeader == null) return false;
        String type = contentTypeHeader.getValue();
        if ("application/json".equals(type)) return true;
        if (!type.startsWith("application/json")) return false;
        char nextChar = type.charAt("application/json".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    @Override
    public Iterator<InDoipSegment> iterator() {
        return Spliterators.iterator(spliterator);
    }

    @Override
    public Spliterator<InDoipSegment> spliterator() {
        return spliterator;
    }

    @Override
    public Stream<InDoipSegment> stream() {
        return StreamSupport.stream(spliterator, false);
    }

    @Override
    public void close() {
        if (in != null) try { in.close(); } catch (IOException ex) { }
        if (entity!=null) EntityUtils.consumeQuietly(entity);
        if (httpResponse != null) try { httpResponse.close(); } catch (IOException ex) { }
    }
}
