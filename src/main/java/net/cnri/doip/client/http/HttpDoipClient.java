package net.dona.doip.client.http;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import net.cnri.util.StringUtils;
import net.dona.doip.*;
import net.dona.doip.client.*;
import net.dona.doip.client.transport.DoipClientResponse;
import net.dona.doip.util.GsonUtility;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class HttpDoipClient extends AbstractDoipClient {

    private String baseUri;
    protected final CloseableHttpClient httpClient;
    private final Gson gson;

    public HttpDoipClient(String serviceId, String ipAddress, int port) throws DoipException {
        this(baseUriFromServiceInfo(new ServiceInfo(serviceId, ipAddress, port)));
    }

    public HttpDoipClient(ServiceInfo serviceInfo) throws DoipException {
        this(baseUriFromServiceInfo(serviceInfo));
    }

    public HttpDoipClient(String baseUri) throws DoipException {
        this.baseUri = baseUri;
        this.httpClient = this.getNewHttpClient();
        this.gson = GsonUtility.getGson();
    }

    @Override
    public void close() {
        HttpClientUtils.closeQuietly(httpClient);
    }

    public DigitalObject create(DigitalObject dobj, AuthenticationInfo authInfo) throws DoipException {
        return create(dobj, authInfo, null);
    }

    @Override
    public DoipClientResponse performOperation(DoipRequestHeaders headers, InDoipMessage input, ServiceInfo serviceInfo) throws DoipException {
        String baseUriForRequest = this.baseUri;
        if (serviceInfo != null) {
            baseUriForRequest = baseUriFromServiceInfo(serviceInfo);
        }
        try {
            if (input != null) {
                return sendMultipartRequest(headers, input, baseUriForRequest);
            } else {
                return sendCompactRequest(headers, baseUriForRequest);
            }
        } catch (Exception e) {
            throw new DoipException(e);
        }
    }

    private DoipClientResponse sendCompactRequest(DoipRequestHeaders headers, String baseUriForRequest) throws DoipException {
        String uri = buildUriForHeaders(headers, baseUriForRequest);
        HttpEntityEnclosingRequestBase httpRequest = new HttpPost(uri);
        if (headers.authentication != null) {
            addCredentials(httpRequest, headers.authentication);
        }
        if (headers.input != null) {
            String json = gson.toJson(headers.input);
            StringEntity body = new StringEntity(json, StandardCharsets.UTF_8);
            body.setContentType("application/json");
            httpRequest.setEntity(body);
        }
        CloseableHttpResponse httpResponse = null;
        try {
            httpResponse = sendHttpRequest(httpRequest);
            return buildDoipClientResponseForHttpResponse(httpResponse);
        } catch (IOException e) {
            if (httpResponse != null) try { httpResponse.close(); } catch (IOException ex) { e.addSuppressed(ex); }
            throw new DoipException(e);
        }
    }

    private String buildUriForHeaders(DoipRequestHeaders headers, String baseUriForRequest) {
        String uri = baseUriForRequest +
                "/?operationId=" + StringUtils.encodeURLComponent(headers.operationId) +
                "&targetId=" + StringUtils.encodeURLComponent(headers.targetId);
        if (headers.attributes != null) {
            String attributesJson = gson.toJson(headers.attributes);
            uri += "&attributes=" + StringUtils.encodeURLComponent(attributesJson);
        }
        return uri;
    }

    private DoipClientResponse sendMultipartRequest(DoipRequestHeaders headers, InDoipMessage input, String baseUriForRequest) throws DoipException, IOException {
        String uri = buildUriForHeaders(headers, baseUriForRequest);
        HttpEntityEnclosingRequestBase httpRequest = new HttpPost(uri);
        if (headers.authentication != null) {
            addCredentials(httpRequest, headers.authentication);
        }
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setContentType(ContentType.create("multipart/mixed"));
        int count = 0;
        for (InDoipSegment segment : input) {
            String defaultName = "part-" + count; //TODO part names?
            count++;
            if (segment.isJson()) {
                JsonElement json = segment.getJson();
                builder.addTextBody(defaultName, json.toString(), ContentType.APPLICATION_JSON);
            } else {
                @SuppressWarnings("resource")
                InputStream in = segment.getInputStream();
                builder.addBinaryBody(defaultName, in);
            }
        }
        httpRequest.setEntity(builder.build());
        CloseableHttpResponse httpResponse = null;
        try {
            httpResponse = sendHttpRequest(httpRequest);
            return buildDoipClientResponseForHttpResponse(httpResponse);
        } catch (IOException e) {
            if (httpResponse != null) try { httpResponse.close(); } catch (IOException ex) { e.addSuppressed(ex); }
            throw new DoipException(e);
        }
    }

    protected CloseableHttpResponse sendHttpRequest(HttpUriRequest request) throws IOException, ClientProtocolException {
        HttpClientContext context = HttpClientContext.create();
        context.setRequestConfig(IGNORE_COOKIES);
        return httpClient.execute(request, context);
    }

    private static final RequestConfig IGNORE_COOKIES = RequestConfig.custom()
            .setCookieSpec(CookieSpecs.IGNORE_COOKIES)
            .setNormalizeUri(false)
            .build();

    public static Map<Integer, String> statusMap = new HashMap<>();
    static {
        statusMap.put(200, DoipConstants.STATUS_OK);
        statusMap.put(400, DoipConstants.STATUS_BAD_REQUEST);
        statusMap.put(401, DoipConstants.STATUS_UNAUTHENTICATED);
        statusMap.put(403, DoipConstants.STATUS_FORBIDDEN);
        statusMap.put(404, DoipConstants.STATUS_NOT_FOUND);
        statusMap.put(409, DoipConstants.STATUS_CONFLICT);
        statusMap.put(400, DoipConstants.STATUS_DECLINED);
        statusMap.put(500, DoipConstants.STATUS_ERROR);
    }

    @SuppressWarnings("resource")
    // the InDoipMessage is closed when the DoipClientResponse is closed
    private DoipClientResponse buildDoipClientResponseForHttpResponse(CloseableHttpResponse httpResponse) throws IOException {
        DoipResponseHeaders initialSegment = initialSegmentFromHttpResponse(httpResponse);
        InDoipMessage in = new InDoipMessageFromHttpResponse(httpResponse);
        DoipClientResponse doipClientResponse = new DoipClientResponse(initialSegment, in);
        doipClientResponse.setOnClose(() -> HttpClientUtils.closeQuietly(httpResponse));
        return doipClientResponse;
    }

    private DoipResponseHeaders initialSegmentFromHttpResponse(CloseableHttpResponse httpResponse) {
        DoipResponseHeaders initialSegment;
        Header doipResponseHeader = httpResponse.getFirstHeader("Doip-Response");
        if (doipResponseHeader != null) {
            String doipResponseHeaderValue = doipResponseHeader.getValue();
            initialSegment = gson.fromJson(doipResponseHeaderValue, DoipResponseHeaders.class);
        } else {
            initialSegment = new DoipResponseHeaders();
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String doipStatus = statusMap.get(statusCode);
            System.out.println(statusCode);
            initialSegment.status = doipStatus;
        }
        return initialSegment;
    }

    private void addCredentials(HttpRequest request, JsonElement authentication) {
//        JsonObject authObj;
//        if (authentication.isJsonObject()) {
//            authObj = authentication.getAsJsonObject();
//            if (authObj.has("password")) {
//                String password = authObj.get("password").getAsString();
//                String username = authObj.get("username").getAsString();
//                addBasicAuthHeader(request, username, password);
//                return;
//            } else if (authObj.has("token")) {
//                String token = authObj.get("token").getAsString();
//                addBearerTokenAuthHeader(request, token);
//                return;
//            }
//        }
        //Always sending Doip Authorization header so as to include asUserId when present
        addDoipAuthHeader(request, authentication);
    }

    private void addDoipAuthHeader(HttpRequest request, JsonElement authentication) {
        String json = gson.toJson(authentication);
        String base64OfJson = Base64.getUrlEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8));
        request.addHeader("Authorization", "Doip " + base64OfJson);
    }

//    private void addBearerTokenAuthHeader(HttpRequest request, String token) {
//        request.addHeader("Authorization", "Bearer " + token);
//    }
//
//    private void addBasicAuthHeader(HttpRequest request, String usernameParam, String passwordParam) {
//        if (usernameParam == null) return;
//        UsernamePasswordCredentials creds = new UsernamePasswordCredentials(usernameParam, passwordParam);
//        try {
//            request.addHeader(new BasicScheme().authenticate(creds, request, null));
//        } catch (AuthenticationException e) {
//            throw new AssertionError(e);
//        }
//    }

    private static String baseUriFromServiceInfo(ServiceInfo serviceInfo) {
        String baseUri = "https://" + serviceInfo.ipAddress + ":" + serviceInfo.port + "/doip/";
        return baseUri;
    }

    @SuppressWarnings("resource")
    protected CloseableHttpClient getNewHttpClient() throws DoipException {
        try {
            SSLConnectionSocketFactory factory = buildSSLConnectionSocketFactory();

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE)
                    .register("https", factory)
                    .build();
            ConnectionConfig connectionConfig = ConnectionConfig.custom()
                    .setCharset(Consts.UTF_8)
                    .build();
//            SocketConfig socketConfig = SocketConfig.custom()
//                    .setSoTimeout(90000)
//                    .build();
            PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            int connections = getMaxConnectionsConfiguration();
            connManager.setDefaultMaxPerRoute(connections);
            connManager.setMaxTotal(connections);
            connManager.setDefaultConnectionConfig(connectionConfig);
//            connManager.setDefaultSocketConfig(socketConfig);

//            RequestConfig requestConfig = RequestConfig.custom()
////                    .setConnectTimeout(30000)
////                    .setSocketTimeout(90000)
//                    .setCookieSpec(CookieSpecs.IGNORE_COOKIES)
//                    .build();

            RequestConfig requestConfig = RequestConfig.custom()
                    .setNormalizeUri(false)
                    .build();

            return HttpClients.custom()
                    .setConnectionManager(connManager)
                    .setDefaultRequestConfig(requestConfig)
                    .build();
        } catch (Exception e) {
            throw new DoipException(e);
        }
    }

    protected int getMaxConnectionsConfiguration() {
        return 200;
    }

    protected SSLConnectionSocketFactory buildSSLConnectionSocketFactory() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, CertificateException, IOException {
        SSLContext sslContext;
        if (Boolean.parseBoolean(System.getProperty("cordra.client.tls.useSystemDefault"))) {
            sslContext = SSLContexts.createSystemDefault();
        } else if (Boolean.parseBoolean(System.getProperty("cordra.client.tls.useDefault"))) {
            sslContext = SSLContexts.createDefault();
        } else {
            String trustStoreProperty = System.getProperty("cordra.client.tls.trustStore");
            if (trustStoreProperty == null) {
                sslContext = SSLContexts.custom().loadTrustMaterial(TrustAllStrategy.INSTANCE).build();
            } else {
                SSLContextBuilder builder = SSLContexts.custom();
                String provider = System.getProperty("cordra.client.tls.trustStoreProvider");
                if (provider != null) {
                    builder.setProvider(provider);
                }
                String type = System.getProperty("cordra.client.tls.trustStoreType");
                if (type != null) {
                    builder.setKeyStoreType(type);
                }
                String algorithm = System.getProperty("cordra.client.tls.TrustManagerFactory.algorithm");
                if (algorithm != null) {
                    builder.setTrustManagerFactoryAlgorithm(algorithm);
                }
                File file = new File(trustStoreProperty);
                String storePassword = System.getProperty("cordra.client.tls.trustStorePassword");
                builder.loadTrustMaterial(file, storePassword.toCharArray());
                sslContext = builder.build();
            }
        }
        String hostnameVerificationProperty = System.getProperty("cordra.client.tls.hostnameVerification");
        HostnameVerifier hostnameVerifier;
        if (hostnameVerificationProperty == null || !Boolean.parseBoolean(hostnameVerificationProperty)) {
            hostnameVerifier = NoopHostnameVerifier.INSTANCE;
        } else {
            hostnameVerifier = SSLConnectionSocketFactory.getDefaultHostnameVerifier();
        }
        SSLConnectionSocketFactory factory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
        return factory;
    }
}
